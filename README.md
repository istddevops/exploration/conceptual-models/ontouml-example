# ontoUML voorbeeld

Voorbeeld van een ontoUML HTML-publicatie op basis van:

- Een ontoUML JSON-formaat export-bestand (via Visual Paradigm ontoUML Plugin)

- Een weergave via plantUML via de online services van [kroki.io]()

- De HUGO Statische Site Generator


## ontoUML Visual Paradigm Datamodel

Een JSON-export vanuit Visual Paradigm is gebaseerd op het [ontoUML Schema](https://github.com/OntoUML/ontouml-schema). Zie onderstaande UML-diagram voor de samenstelling en inhoud van een ontoUML-project.

```plantuml
@startuml
hide methods

package "ontoUML-uitwisselingsformaat" {

    abstract Object {
        id
        name
        description
    }

    class Package {
        propertyAssignments
    }

    class Project

    
    
    package "Model" {
        
    }

    package "Diagrammen" {
    
    }
    
    Object <|-- Project
    Object <|-- Package

    Project o-- "Model"
    Project o-- "Diagrammen"
}


@enduml
```

Het **ontoUML-uitwisselingsformaat**  is ongebouwd uit:

- Een generieke **Object**-klasse met de basis-attributen ter identificatie (id) en beschrijving (name, description)
- De **Project**-klasse die het gehele uitwisselingsbestand beschrijft en is opgedeeld naar een **model**- en **diagrammen**-pakket
- Met de **Package**-klasse worden het **Model** en de **Diagrammen** pakket beschreven (zie verder onderliggende UML-diagrammen)



```plantuml
@startuml
hide methods

class model {

}
        
package "Model" {
        
    class Package {
                
    }
            
            abstract modelElement {
                stereotype
            }
            
            class Class {
                restrictedTo
            }

            class Generalization {
            }
            

            class GeneralizationSet {
                isDisjoint
                isComplete
            }
            
            class Relation {
                aggregationKind
            }


            Package o-- "*" Package: contents
            Package o-- "*" modelElement: contents

            modelElement <|-- Class
            modelElement <|-- Generalization
            modelElement <|-- GeneralizationSet
            modelElement <|-- Relation

            Generalization -> Class: general
            Generalization -> Class: specific
            GeneralizationSet o- "*" Generalization
            

}
    
model o-- Package: contents

@enduml
```

Een **model**-klasse is bevat **contents** dat meerdere **Package**-klassen kan bevatten met daarin:

- **contents** dat meerdere **Package**-klassen en/of **modelElement**-klassen kan beavetten. 
- Een **modelElement**-object wordt ingedeeld naar een *sub-klasse* die is gerelateerd aan de (onto)UML-modelleringsmethode (Class, Generalization, Relation, ...).