# Notes

## PlantUML

```plantuml
@startuml
!theme spacelab
class Test1
class Test2
class Test3
circle test

Test1 <|-d- Test2
Test3 -[hidden] Test1

@enduml
```

- Er kan een `[hidden]` link worden toegevoegd voor de *horizontale positie*
- Idee is om een `[hidden]` link toe te voegen wanneer er :
  1. Geen Generatisatie, Associatie of Relatie-Link voor een `Klasse(i)` is gedefinieerd
  2. De `Klasse(i)` op een *horizontale lijn* links of rechts van een andere `Klasse(j)` is gepositioneerd in het *diagram*

